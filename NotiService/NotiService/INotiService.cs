﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml.Linq;

namespace NotiSvc
{
    // 참고: "리팩터링" 메뉴에서 "이름 바꾸기" 명령을 사용하여 코드 및 config 파일에서 인터페이스 이름 "IService1"을 변경할 수 있습니다.
    [ServiceContract]
    public interface INotiService
    {

        //[OperationContract]
        //string GetData(int value);

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: 여기에 서비스 작업을 추가합니다.

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "request?json={jsonStr}", BodyStyle = WebMessageBodyStyle.Bare)]
        XElement HandlePaperAndNoti(string jsonStr);

        //param = '"{\'DBName\':\'mobileappdev_bis\',\'NotiYN\':1,\'Type\':0,\'NotiTitle\':\'[공지]\',\'NotiBody\':\'테스트\',\'CreatedBy\':2,\'CompanySeq\':1,\'OperationType\':1,\'Receivers\':[\'301\']}"'
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "postRequest", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Xml)]
        XElement postRequest(CMPYParam param);

        //{"jsonStr":"{\"DsnBis\":\"mobileappdev_bis\",\"DsnOper\":\"mobileappdev_oper\",\"NotiYN\":1,\"CompanySeq\":1,\"NotiTitle\":\"postman\",\"NotiBody\":\" test test test \n\",\"CreatedBy\":\"50345\",\"Receivers\":[24236],\"NotSaveToDB\":1}"}
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "erpMessageRequest", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Xml)]
        XElement erpMessageRequest(ERPParam param);

        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "postTest", BodyStyle = WebMessageBodyStyle.Bare,RequestFormat =WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Xml)]
        //XElement postTest(Param param);

        //[OperationContract]
        //[WebInvoke(Method = "GET", UriTemplate = "callSP")]
        //string callSP();
    }


    //// 아래 샘플에 나타낸 것처럼 데이터 계약을 사용하여 복합 형식을 서비스 작업에 추가합니다.
    [DataContract]
    public class ERPParam
    {
        [DataMember(Name = "DsnBis", IsRequired = true)]
        public string DsnBis { get; set; }

        [DataMember(Name = "DsnOper", IsRequired = true)]
        public string DsnOper { get; set; }

        [DataMember]
        public int NotiYN { get; set; }

        [DataMember]
        public int IsOfficial { get; set; }

        [DataMember]
        public int CompanySeq { get; set; }

        [DataMember(Name = "NotiTitle", IsRequired = true)]
        public string NotiTitle { get; set; }

        [DataMember]
        public string NotiBody { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public List<string> Receivers { get; set; }

        [DataMember]
        public int NotSaveToDB { get; set; }

        [DataMember]
        public string PgmSeq { get; set; }

        [DataMember]
        public string ExecuteMethodName { get; set; }

        [DataMember]
        public List<List<string>> FormData { get; set; }
    }

    [DataContract]
    public class CMPYParam
    {
        //[DataMember(Name = "json", IsRequired = true)]
        [DataMember]
        public string jsonStr { get; set; }
    }
}
