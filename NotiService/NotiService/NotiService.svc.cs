﻿using NotiSvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Configuration;
using AngKor.Ylw.Common;
using System.Data;
using System.Xml.Linq;
using System.Xml;

namespace NotiSvc
{
    // 참고: "리팩터링" 메뉴에서 "이름 바꾸기" 명령을 사용하여 코드, svc 및 config 파일에서 클래스 이름 "Service1"을 변경할 수 있습니다.
    // 참고: 이 서비스를 테스트하기 위해 WCF 테스트 클라이언트를 시작하려면 솔루션 탐색기에서Service1.svc나 Service1.svc.cs를 선택하고 디버깅을 시작하십시오.
    public class NotiService : INotiService
    {
        string SP_CreatePaper = "_SCAMobile_CreatePaper";
        string SP_CreateVotePaper = "_SCAMobile_CreateVotePaper";
        string SP_CreateSchedulePaper = "_SCAMobile_CreateSchedulePaper";
        string SP_UpdatePaper = "_SCAMobile_UpdatePaper";
        string SP_UpdateSchedulePaper = "_SCAMobile_UpdateSchedulePaper";

        string SP_VmServiceUrlQuery = "_SCAMobileVmServiceUrlQuery";
        string SP_NotiValidation = "_SCAMobileNotiValidation";

        string SP_NotiLog = "_SCAMobileSavePaperNotiLogs";

        string SP_ERPCreateMessage = "_SCAMobile_SaveUserNotification";
        string SP_ERPNotiLog = "_SCAMobileSaveERPNotiLogs";
        string SP_ERPNotiValidation = "_SCAMobileERPNotiValidation";

        string SINGLEQUOTE_KEY = "SINGLEQUOTE_KEY";

        //for erp message notification
        public XElement erpMessageRequest(ERPParam param)
        {
            try
            {
                //lua에서 '값을 전달 못함.. 치환하여 해결
                //string jsonStr = "";
                //if (!string.IsNullOrEmpty(param.jsonStr))
                //{
                //    jsonStr = param.jsonStr.Replace(SINGLEQUOTE_KEY, @"'");
                //}
                return handleERPMessageAndNoti(param);
            }
            catch (Exception e)
            {
                return createErrorDS("ERPMessageRequest : " + e.ToString());
            }
        }
        public XElement handleERPMessageAndNoti(ERPParam param)
        {
            //{"DsnOper":"mobileappdev_oper","DsnBis":"mobileappdev_bis","NotiTitle":"[official message]", "NotiBody":"Noti type example","NotiYN":1,"IsOfficial":1,"CreatedBy":520001,"CompanySeq":1,"Receivers":"[\"0\",\"24236\",\"50345\"]","NotSaveToDB":1,"Data":{ "PgmSeq":501018} }
            //JObject param;
            //try
            //{
            //    param = JObject.Parse(jsonStr);
            //}
            //catch (Exception e)
            //{
            //    return createErrorDS("JObject.Parse(paramERPMessageRequest) : " + e.ToString());
            //}

            //알림유무 - 기본은 알림 안보내기
            int NotiYN = param.NotiYN;

            //공지 유무 - 기본은 공지 아님
            int IsOfficial = param.IsOfficial;

            DataSet returnDS = new DataSet("NewDataSet");
            string paramStr = "";
            string spName = "";
            string messageSeq = "";

            string DsnBis = param.DsnBis;
            string CompanySeq = param.CompanySeq.ToString();
            string CreatedBy = param.CreatedBy;

            string receiversXML = "";
            if (IsOfficial == 1)
            {
                receiversXML = "null";
            }
            else
            {
                for (int i = 0; i < param.Receivers.Count; i++)
                {
                    receiversXML += "<Receiver>" + param.Receivers[i] + "</Receiver>";
                }
            }


            string NotiTitle = replace(param.NotiTitle);

            string NotiBody = replace(param.NotiBody);

            //1이면 paper에 저장하지 않는다. - 기본은 저장
            int notSaveToDB = param.NotSaveToDB;
            //sp 결과 저장
            string spResultStr = "";

            if (notSaveToDB != 1)
            {
                string PgmSeq = param.PgmSeq;

                if (PgmSeq != null)
                {
                    PgmSeq = "'" + PgmSeq + "'";
                }
                else
                {
                    PgmSeq = "null";
                }

                string FormData = null;
                try
                {
                    if (param.FormData != null)
                    {
                        FormData = JsonConvert.SerializeObject(param.FormData);
                        FormData = "'" + FormData + "'";
                    }
                    else
                    {
                        FormData = "null";
                    }
                }
                catch (Exception e)
                {
                    //화면 정보 없는 메세지의 경우...
                }

                string ExecuteMethodName = param.ExecuteMethodName;

                if (ExecuteMethodName != null)
                {
                    ExecuteMethodName = "'" + ExecuteMethodName + "'";
                }
                else
                {
                    ExecuteMethodName = "null";
                }

                paramStr = "'" + CreatedBy + "', '" + receiversXML + "', '" + NotiTitle + "', '" + NotiBody +"'," + PgmSeq + ", " + FormData + ", " + ExecuteMethodName;

                spName = SP_ERPCreateMessage;

                //db에 저장한 경우 아래 변수는 더 이상 쓸일이 없음.
                receiversXML = "";

                try
                {
                    if (!string.IsNullOrEmpty(DsnBis))
                    {
                        if (!string.IsNullOrEmpty(spName))
                        {
                            var spResult = callSP(spName, paramStr, DsnBis);
                            spResultStr = spResult.ToString();
                            returnDS = JsonConvert.DeserializeObject<DataSet>(spResult.ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    //make errorDS
                    return createErrorDS(spName + " : " + spResultStr + "\n" + e.ToString());
                }


                try
                {
                    spResultStr = returnDS.ToString();
                    messageSeq = returnDS.Tables[0].Rows[0]["MessageSeq"].ToString();
                    if (!string.IsNullOrEmpty(messageSeq))
                    {
                        //성공
                    }
                    else
                    {
                        //실패
                        //make errorDS
                        return createErrorDS(spName + " : " + spResultStr);
                    }
                }
                catch (Exception e)
                {
                    //실패
                    //make errorDS
                    return createErrorDS(spName + " : " + spResultStr + "\n" + e.ToString());
                }
            }
            else
            {
                messageSeq = null;
            }
            //check empSeq, get tokens by notiYN
            //string tokens = "";
            //string empNotExistMsg = "";
            //string senderName = "";
            //string emps = "";



            try
            {
                string VmServiceUrl = "";
                string DsnOper = param.DsnOper;
                if (!string.IsNullOrEmpty(DsnOper))
                {
                    //운영DB의 _TCAMobileSecurityPolicy 테이블에서 VmServiceUrl 값 가져오기
                    var spResult = callSP(SP_VmServiceUrlQuery, CompanySeq, DsnOper);
                    spResultStr = spResult.ToString();
                    DataSet VmServiceUrlQueryDS = JsonConvert.DeserializeObject<DataSet>(spResult.ToString());
                    VmServiceUrl = (VmServiceUrlQueryDS.Tables[0].Rows[0]).ItemArray[0].ToString();
                }

                //               _SCAMobileNotiValidation

                //@CompanySeq         INT = 1
                //,@Sender            INT
                //,@NotiYN            INT
                //,@IsOfficial        INT
                //,@OperationType     INT
                //,@PaperID           INT = -1
                //,@Receivers         XML

                JObject validation = null;
                if (!string.IsNullOrEmpty(VmServiceUrl) && NotiYN == 1)
                {
                    validation = callSP(SP_ERPNotiValidation, "'" + CompanySeq + "', '" + CreatedBy + "', '" + IsOfficial + "', '" + messageSeq + "', '" + receiversXML + "'", DsnBis);
                }
                //{{
                //    "result": [
                //        {
                //            "empSeq": 176,
                //            "tokenList": "",
                //            "unreadCount": 20
                //        },
                //        {
                //            "empSeq": 181,
                //            "tokenList": "['token1','token2','token3']",
                //            "unreadCount": 15
                //        }
                //    ],
                //    "result1": [
                //        {
                //            "sender": "사원_1260"
                //        }
                //    ]
                //}}

                //badge 기능을 추가하면서 사원 별로 token 서비스를 호출해야 함
                foreach (var item in ((JArray)validation["Result"]))
                {
                    string receiverSeq = (string)item["UserSeq"];
                    string tokenList = (string)item["TokenList"];
                    string unreadCount = (string)item["UnreadCount"];

                    string senderName = (string)validation["Result1"].First["Sender"];

                    //token is exist, call sendNotification
                    if (!(string.IsNullOrEmpty(tokenList)))
                    {
                        string tokenConvert = JsonConvert.SerializeObject(JArray.Parse(tokenList));
                        //send notification to VM made by subhaye *messageSeq = paperID // pgmseq 생략
                        sendNotification(SP_ERPNotiLog, VmServiceUrl, param.NotiTitle, param.NotiBody, tokenConvert, unreadCount, null, messageSeq, null, senderName, CompanySeq, CreatedBy, receiverSeq, DsnBis);
                    }
                }
            }
            catch
            {
                //make errorDS
                //returnDS = createErrorDS(_SCAMobileNotiValidation + " : " + e.ToString());
            }

            return addXMLHeader(returnDS.GetXml());// + "request test";
        }

        public void sendNotification(string logSPName, string vmServiceURL, string title, string body, string tokens, string unreadCount, string pgmSeq, string paperID, string edit, string senderName, string companySeq, string senderSeq, string receiverSeq, string dbName)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(vmServiceURL);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            //var result = "test";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                //token api 호출을 위해 필요한 값
                JObject jsonInfo = new JObject();
                JObject notificationJson = new JObject();
                JObject dataJson = new JObject(); //optional
                try
                {
                    notificationJson.Add("title", title);
                    notificationJson.Add("body", body);
                    notificationJson.Add("badge", unreadCount);
                    jsonInfo.Add("notification", notificationJson);
                    jsonInfo.Add("device_token", tokens);

                    bool isUseDataParam = false;
                    if (!(string.IsNullOrEmpty(pgmSeq)))
                    {
                        isUseDataParam = true;
                        dataJson.Add("pgmSeq", pgmSeq); //optional
                    }

                    if (!(string.IsNullOrEmpty(paperID)))
                    {
                        isUseDataParam = true;
                        dataJson.Add("paperKey", paperID); //optional
                    }

                    if (!(string.IsNullOrEmpty(edit)))
                    {
                        isUseDataParam = true;
                        //update paper - 1 else - ''
                        dataJson.Add("edit", edit); //optional
                    }

                    if (!(string.IsNullOrEmpty(senderName)))
                    {
                        isUseDataParam = true;
                        dataJson.Add("sender", senderName); //optional
                    }

                    if (isUseDataParam)
                    {
                        jsonInfo.Add("data", dataJson); //optional
                    }
                }
                catch (Exception e)
                {
                    //result = e.ToString();
                }

                streamWriter.Write(jsonInfo);
                streamWriter.Flush();
                streamWriter.Close();
            }

            string result = "";
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpResponse.StatusCode == HttpStatusCode.OK) {
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                    }
                }
            }
            catch {}

            try
            {
                JObject json = JObject.Parse(result);


                //로그 저장을 위해 필요한 값
                JArray tokenArr;

                try
                {
                    tokenArr = JArray.Parse(tokens);
                }
                catch
                {
                    tokenArr = new JArray();
                }

                //string[] stringSeparators = new string[] { "~," };
                //string[] receiverArr = receivers.Split(stringSeparators, StringSplitOptions.None);

                int index = 0;

                foreach (var child in json.GetValue("results").Children())
                {
                    //sp 호출 
                    //exec _SCAMobileSavePaperLogs @CompanySeq, @PaperID, @Sender. @Receiver, @IsSuccess, @Remark(error code)
                    //exec _SCAMobileSavePaperNotiLogs '1','1044','2662','22222','0','InvalidRegistration'

                    //noti error가 발생했는지 값 체크
                    string errorMsg = child.Value<string>("error");

                    //응답코드에 해당하는 Receiver EmpSeq 확인
                    string empSeq = receiverSeq;//"-1"; //Not Match
                    //if (receiverArr.Count() > index)
                    //{
                    //    empSeq = receiverArr[index];
                    //}

                    //응답코드에 해당하는 Receiver Token 확인
                    string token = ""; //Not Match
                    if (tokenArr.Count() > index)
                    {
                        token = (string)tokenArr[index];
                    }

                    //오류코드가 있으면 노티 실패
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        //noti fail
                        //call sp
                        callSP(logSPName, "'" + companySeq + "', '" + paperID + "', '" + senderSeq + "', '" + empSeq + "', '" + token + "', '0', '" + errorMsg + "'", dbName);
                    }
                    //없으면 성공
                    else
                    {
                        //noti success
                        //call sp
                        callSP(logSPName, "'" + companySeq + "', '" + paperID + "', '" + senderSeq + "', '" + empSeq + "', '" + token + "', '1', ''", dbName);
                    }
                    index++;
                }
            }
            catch {}

            //return result;
        }
        public void saveNotiLog(string CompanySeq, string PaperID, string Sender, string Receiver, string IsSuccess, string Remark, string dbName)
        {
        }
        //public XElement postTest(Param param)
        //{
        //    return HandlePaperAndNoti(param.jsonStr);
        //}
        public XElement postRequest(CMPYParam param)
        {
            try
            {
                //lua에서 '값을 전달 못함.. 치환하여 해결
                string jsonStr = "";
                if (!string.IsNullOrEmpty(param.jsonStr))
                {
                    jsonStr = param.jsonStr.Replace(SINGLEQUOTE_KEY, @"'");
                    jsonStr = jsonStr.Replace(@"\'", @"'");
                }
                return HandlePaperAndNoti(jsonStr);
            }
            catch (Exception e)
            {
                return createErrorDS("postRequest : " + e.ToString());
            }
        }
        //http://localhost:49452/NotiService.svc/requestTest?json={'notification' : {'body' : 'json body text', 'title' : 'json title text' }, 'data' : {'paperKey': 123, 'pgmSeq': 500547  },   'token' : ['ehWlcM1oL48:APA91bE1WaQgZj8NeYZYps5t0uolloL2xUmhtQiEvKbjs9etplKQWoPjy2PCFAZrh3xcon6u-GJHS1A5kCwunVwuzPIOMDMrLtQ7O2faXpiHcIq5Sqjmz9SpG6DG06mraikAzGDhyYL13ybA5rel5Rpo982ounAnnw','test']}
        //http://wbs.ksystem.co.kr:8889/notiKsystemSvc/NotiService.svc/requestTest?json={'Type':2,'NotiBody':'Noti type example','CreatedBy':301,'CompanySeq':1,'Receivers':['0','4639','4638','4634'],'OperationType':0,'Data':{'PgmSeq':501018}}
        public XElement HandlePaperAndNoti(string jsonStr)
        {
            //{ "Type":2,"NotiBody":"Noti type example","CreatedBy":301,"CompanySeq":1,"Receivers":"[\"0\",\"4639\",\"4638\",\"4634\"]","OperationType":0,"Data":{ "PgmSeq":501018} }
            JObject param;
            try
            {
                param = JObject.Parse(jsonStr);
            }
            catch (Exception e)
            {
                return createErrorDS("JObject.Parse(parampostRequest) : " + e.ToString());
            }

            //OperationType (1 -> Create, 2 - > Edit) -> 없으면 알림만 보내는거 (notiyn 1일때)
            string OperationType = (string)param.GetValue("OperationType");

            //Type ( 1107001 -> Normal paper, 1107002 -> Vote paper, 1107003 -> Action button)
            string Type = (string)param.GetValue("Type");

            //알림유무
            string NotiYN = (string)param.GetValue("NotiYN");

            //공지 유무
            string isOfficial = (string)param.GetValue("IsOfficial");
            if (string.IsNullOrEmpty(isOfficial))
            {
                isOfficial = "0";
            }
            
            DataSet returnDS = new DataSet("NewDataSet");
            string paramStr = "";
            string spName = "";
            string DsnBis = "";
            string CompanySeq = (string)param["CompanySeq"];
            string PaperId = "";
            string PgmSeq;
            try
            {
                PgmSeq = (string)param["PgmSeq"];
            }
            catch (Exception e)
            {
                PgmSeq = "";
            }

            string CreatedBy = (string)param["CreatedBy"];
            string isEdit = "";


            string receiversXML = "";
            if (isOfficial == "1")
            {
                receiversXML = "null";
            }
            else
            {
                JArray receivers;


                try
                {
                    receivers = JArray.Parse(JsonConvert.SerializeObject(param["Receivers"]));
                }
                catch
                {
                    receivers = new JArray();
                }


                for (int i = 0; i < receivers.Count; i++)
                {
                    receiversXML += "<Receiver>" + receivers[i] + "</Receiver>";
                }
            }

            switch (OperationType)
            {
                case null://Not handle paper
                    break;
                case "1": //Create
                    {
                        string Title = replace((string)param["NotiBody"]);
                        string HashTagXml = (string)param["HashTagXml"];
                        //181227
                        string MultiReceiver = replace((string)param["MultiReceiver"]);
                        string LabelDeptSeq = (string)param["LabelDeptSeq"];
                        string OpinionEnabled = (string)param["OpinionEnabled"];
                        string RemarkScript = replace((string)param["RemarkScript"]);

                        //Normal Paper : 1107001
                        //Vote type Paper: 1107002
                        //Pgm Open Type: 1107003
                        //Schedule Type : 1107004
                        switch (Type)
                        {
                            case "1107001": //Normal
                            case "1107003": //Pgm Open
                                {
                                    //@CompanySeq INT,
                                    //@Title nvarchar(max),
                                    //@CreatedBy int,
                                    //@ReceiversXml xml,
                                    //@AttachmentXml xml,
                                    //@HashTagXml xml,
                                    //@PgmSeq int,
                                    //@IsOfficial INT,
                                    //@PaperType int,
                                    //@NotiYN bit,
                                    //@MultiReceiver bit,
                                    //@LabelDeptSeq int

                                    string AttachmentXml = replace((string)param["AttachmentXml"]);

                                    paramStr = "'" + CompanySeq + "', '" + Title + "', '" + CreatedBy + "', '" + receiversXML + "', '" + AttachmentXml + "', '" + HashTagXml + "', '" + PgmSeq + "', '" + isOfficial + "', '" + Type + "', '" + NotiYN + "', '" + MultiReceiver + "', '" + LabelDeptSeq + "'";
                                    spName = SP_CreatePaper;

                                    break;
                                }
                            case "1107002": //Vote
                                {
                                    //   @CompanySeq INT,
                                    //   @Title varchar(MAX),
                                    //   @CreatedBy int,
                                    //   @VoteClosingTime varchar(50),
                                    //   @VoteSelectionType tinyint,
                                    //   @VoteVisibilityType tinyint,
                                    //   @CanAddItem BIT,
                                    //   @HasVoteLimit BIT,
                                    //   @VoteItemType int,
                                    //   @ReceiversXml xml,
                                    //   @VoteXml xml,
                                    //   @HashTagXml xml,
                                    //   @IsOfficial INT,
                                    //   @PaperType int,
                                    //   @NotiYN bit,
                                    //   @MultiReceiver bit,
                                    //   @LabelDeptSeq int,
                                    //   @OpinionEnabled bit,
                                    //   @RemarkScript nvarchar(max)
                                    string VoteClosingTime = (string)param["VoteClosingTime"];
                                    string VoteSelectionType = (string)param["VoteSelectionType"];
                                    string VoteVisibilityType = (string)param["VoteVisibilityType"];
                                    string CanAddItem = (string)param["CanAddItem"];
                                    string HasVoteLimit = (string)param["HasVoteLimit"];
                                    string VoteItemType = (string)param["VoteItemType"];
                                    string VoteXml = replace((string)param["VoteXml"]);

                                    paramStr = "'" + CompanySeq + "', '" + Title + "', '" + CreatedBy + "', '" + VoteClosingTime + "', '" + VoteSelectionType + "', '" + VoteVisibilityType + "', '" + CanAddItem + "', '" + HasVoteLimit + "', '" + VoteItemType + "', '" + receiversXML + "', '" + VoteXml + "', '" + HashTagXml + "', '" + isOfficial + "', '" + Type + "', '" + NotiYN + "', '" + MultiReceiver + "', '" + LabelDeptSeq + "', '" + OpinionEnabled + "', '" + RemarkScript + "'";
                                    spName = SP_CreateVotePaper;
                                    //"\"{\\\"result\\\":[{\\\"paperID\\\":293,\\\"title\\\":\\\"Vote for next workshop destination.\\\",\\\"pgmSeq\\\":null,\\\"paperType\\\":null,\\\"voteClosingTime\\\":null,\\\"voteSelectionType\\\":1,\\\"voteVisibilityType\\\":1,\\\"createdBy\\\":2,\\\"empSeq\\\":77,\\\"createdByName\\\":\\\"이원석\\\",\\\"createdDate\\\":\\\"2018-08-23T07:18:13.9576591\\\",\\\"updatedBy\\\":null,\\\"updatedDate\\\":\\\"2018-08-23T07:18:13.9576591\\\",\\\"hasVoteLimit\\\":1,\\\"canAddItem\\\":1,\\\"voteItemType\\\":1,\\\"isPinned\\\":null,\\\"feedbackValue\\\":null,\\\"feedbackCount\\\":0,\\\"paperAttachments\\\":null,\\\"voteItems\\\":\\\"<VoteItem><VoteID>187792</VoteID><VoteTitle>Seoul</VoteTitle><VoteLimit>5</VoteLimit><UserVote>0</UserVote><VoteAttachments>&lt;VoteAttachment&gt;&lt;AttachmentID&gt;129698&lt;/AttachmentID&gt;&lt;AttachType&gt;3&lt;/AttachType&gt;&lt;LocationName&gt;&lt;/LocationName&gt;&lt;Longitude&gt;126.977969200&lt;/Longitude&gt;&lt;Latitude&gt;37.566535000&lt;/Latitude&gt;&lt;/VoteAttachment&gt;</VoteAttachments></VoteItem><VoteItem><VoteID>187793</VoteID><VoteTitle>Busan</VoteTitle><VoteLimit>10</VoteLimit><UserVote>0</UserVote><VoteAttachments>&lt;VoteAttachment&gt;&lt;AttachmentID&gt;129699&lt;/AttachmentID&gt;&lt;AttachType&gt;3&lt;/AttachType&gt;&lt;LocationName&gt;&lt;/LocationName&gt;&lt;Longitude&gt;129.075641600&lt;/Longitude&gt;&lt;Latitude&gt;35.179554300&lt;/Latitude&gt;&lt;/VoteAttachment&gt;</VoteAttachments></VoteItem>\\\",\\\"voteCounts\\\":null}]}\""
                                    break;
                                }
                            case "1107004": //Schedule
                                {
                                    //@CompanySeq INT,
                                    //@Title varchar(255),
                                    //@CreatedBy int,
                                    //@VoteVisibilityType tinyint,
                                    string VoteVisibilityType = replace((string)param["VoteVisibilityType"]);
                                    //@HasVoteLimit BIT,
                                    string HasVoteLimit = (string)param["HasVoteLimit"];
                                    //@ReceiversXml xml,
                                    //@VoteXml xml,
                                    string VoteXml = replace((string)param["VoteXml"]);
                                    //@HashTagXml xml,
                                    //@IsOfficial INT,
                                    //@NotiYN bit,
                                    //@OpinionEnabled bit,
                                    //@EventTitle nvarchar(max),
                                    string EventTitle = replace((string)param["EventTitle"]);
                                    //@EventBeginTime datetime2(3),
                                    string EventBeginTime = replace((string)param["EventBeginTime"]);
                                    //@EventEndTime datetime2(3),
                                    string EventEndTime = replace((string)param["EventEndTime"]);
                                    //@LocationName nvarchar(255),
                                    string LocationName = replace((string)param["LocationName"]);
                                    //@Latitude decimal(12, 9),
                                    string Latitude = replace((string)param["Latitude"]);
                                    //@Longitude decimal(12, 9),
                                    string Longitude = replace((string)param["Longitude"]);
                                    //@AllDayEvent bit
                                    string AllDayEvent = replace((string)param["AllDayEvent"]);
                                    //@VoteClosingDate
                                    string VoteClosingTime = (string)param["VoteClosingTime"];
                                    //   @MultiReceiver bit,
                                    //   @LabelDeptSeq int

                                    paramStr = "'" + CompanySeq + "', '" + Title + "', '" + CreatedBy + "', '" + VoteVisibilityType + "', '" + HasVoteLimit + "', '" + receiversXML + "', '" + VoteXml + "', '" + HashTagXml + "', '" + isOfficial + "', '" + NotiYN + "', '" + OpinionEnabled + "', '" + EventTitle + "', '" + EventBeginTime + "', '" + EventEndTime + "', '" + LocationName + "', '" + Latitude + "', '" + Longitude + "', '" + AllDayEvent + "', '" + VoteClosingTime + "', '" + MultiReceiver + "', '" + LabelDeptSeq + "', '" + RemarkScript + "'";
                                    spName = SP_CreateSchedulePaper;
                                    break;
                                }
                            default:
                                //returnDS = createErrorDS("Type is not correct");
                                break;
                        }
                        break;
                    }
                case "2": //Edit
                    {
                        isEdit = "1";


                        PaperId = (string)param["PaperId"];
                        string Title = replace((string)param["NotiBody"]);
                        string HashTagXml = replace((string)param["HashTagXml"]);

                        //Normal Paper : 1107001
                        //Vote type Paper: 1107002
                        //Pgm Open Type: 1107003
                        //Schedule Type : 1107004
                        switch (Type)
                        {
                            case "1107001": //Normal

                                //   @CompanySeq INT,
                                //   @CreatedBy INT,
                                //@PaperId int,
                                //   @Title varchar(max),
                                //@NewAttachmentXml xml,
                                //   @DeleteAttachmentXml xml,
                                //@HashTagXml xml
                                //@UpdatedScheduleXml xml
                                //string CreatedBy_e = (string)param["CreatedBy"];
                                string NewAttachmentXml = replace((string)param["NewAttachmentXml"]);
                                string DeleteAttachmentXml = replace((string)param["DeleteAttachmentXml"]);
                                string UpdatedScheduleXml = replace((string)param["UpdatedScheduleXml"]);
                                paramStr = "'" + CompanySeq + "', '" + CreatedBy + "', '" + PaperId + "', '" + Title + "', '" + NewAttachmentXml + "', '" + DeleteAttachmentXml + "', '" + HashTagXml + "', '" + NotiYN + "', '" + UpdatedScheduleXml + "'";
                                spName = SP_UpdatePaper;

                                break;
                            case "1107004": //Schedule

                                //@PaperId int,
                                //@CompanySeq INT,
                                //@Title nvarchar(max),
                                //@CreatedBy int,
                                //@HashTagXml xml,
                                //@NotiYN bit,
                                //@EventTitle nvarchar(max),
                                //@EventBeginTime datetime2(3),
                                //@EventEndTime datetime2(3),
                                //@LocationName nvarchar(255),
                                //@Latitude decimal(12, 9),
                                //@Longitude decimal(12, 9),
                                //@AllDayEvent bit,
                                //@VoteClosingTime varchar(50)
                                string EventTitle = replace((string)param["EventTitle"]);
                                string EventBeginTime = replace((string)param["EventBeginTime"]);
                                string EventEndTime = replace((string)param["EventEndTime"]);
                                string LocationName = replace((string)param["LocationName"]);
                                string Latitude = replace((string)param["Latitude"]);
                                string Longitude = replace((string)param["Longitude"]);
                                string AllDayEvent = replace((string)param["AllDayEvent"]);
                                string VoteClosingTime = replace((string)param["VoteClosingTime"]); 
                                string RemarkScript = replace((string)param["RemarkScript"]); 

                                 paramStr = "'" + PaperId + "', '" + CompanySeq + "', '" + Title + "', '" + CreatedBy + "', '" + HashTagXml + "', '" + NotiYN + "', '" + EventTitle + "', '" + EventBeginTime + "', '" + EventEndTime + "', '" + LocationName + "', '" + Latitude + "', '" + Longitude + "', '" + AllDayEvent + "', '" + VoteClosingTime + "', '" + RemarkScript + "'";
                                spName = SP_UpdateSchedulePaper;

                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case "3": //reNoti
                    PaperId = (string)param["PaperId"];
                    break;
                default:
                    //returnDS = createErrorDS("OperationType is not correct");
                    break;
            }
            string spResultStr = "";
            try
            {
                DsnBis = (string)param["DsnBis"];
                if (!string.IsNullOrEmpty(DsnBis))
                {
                    if (!string.IsNullOrEmpty(spName))
                    {
                        var spResult = callSP(spName, paramStr, DsnBis);
                        spResultStr = spResult.ToString();
                        returnDS = JsonConvert.DeserializeObject<DataSet>(spResult.ToString());
                    }
                }

                //구버전
                //dbName = (string)param["DBName"];
                //if (!string.IsNullOrEmpty(dbName))
                //{
                //    if (!string.IsNullOrEmpty(spName))
                //    {
                //        var spResult = callSP(spName, paramStr, dbName);
                //        spResultStr = spResult.ToString();
                //        returnDS = JsonConvert.DeserializeObject<DataSet>(spResult.ToString());
                //    }
                //}
            }
            catch (Exception e)
            {
                //make errorDS
                return createErrorDS(spName + " : " + spResultStr + "\n" + e.ToString());
            }
            //check empSeq, get tokens by notiYN
            //string tokens = "";
            //string empNotExistMsg = "";
            //string senderName = "";
            //string emps = "";


            try
            {
                if (string.IsNullOrEmpty(PaperId))
                {
                    PaperId = returnDS.Tables[0].Rows[0]["PaperID"].ToString();
                }
            }
            catch { }

            try
            {
                string VmServiceUrl = "";
                string DsnOper = (string)param["DsnOper"];
                if (!string.IsNullOrEmpty(DsnOper))
                {
                    //운영DB의 _TCAMobileSecurityPolicy 테이블에서 VmServiceUrl 값 가져오기
                    var spResult = callSP(SP_VmServiceUrlQuery, CompanySeq, DsnOper);
                    spResultStr = spResult.ToString();
                    DataSet VmServiceUrlQueryDS = JsonConvert.DeserializeObject<DataSet>(spResult.ToString());
                    VmServiceUrl = (VmServiceUrlQueryDS.Tables[0].Rows[0]).ItemArray[0].ToString();
                }

                //               _SCAMobileNotiValidation

                //@CompanySeq         INT = 1
                //,@Sender            INT
	            //,@NotiYN            INT
                //,@IsOfficial        INT
	            //,@OperationType     INT
                //,@PaperID           INT = -1
	            //,@Receivers         XML
                JObject validation = null;
                if (!string.IsNullOrEmpty(VmServiceUrl))
                {
                    validation = callSP(SP_NotiValidation, "'" + CompanySeq + "', '" + CreatedBy + "', '" + NotiYN + "', '" + isOfficial + "', '" + OperationType + "', '" + PaperId + "', '" + receiversXML + "'", DsnBis);
                }
                //{{
                //    "result": [
                //        {
                //            "empSeq": 176,
                //            "tokenList": "",
                //            "unreadCount": 20
                //        },
                //        {
                //            "empSeq": 181,
                //            "tokenList": "['token1','token2','token3']",
                //            "unreadCount": 15
                //        }
                //    ],
                //    "result1": [
                //        {
                //            "sender": "사원_1260"
                //        }
                //    ]
                //}}

                //badge 기능을 추가하면서 사원 별로 token 서비스를 호출해야 함
                foreach (var item in ((JArray)validation["Result"]))
                {
                    string receiverSeq = (string)item["EmpSeq"];
                    string tokens = (string)item["TokenList"];
                    string unreadCount = (string)item["UnreadCount"];

                    string senderName = (string)validation["Result1"].First["Sender"];

                    //token is exist, call sendNotification
                    if (!(string.IsNullOrEmpty(tokens)))
                    {
                        string tokenConvert = JsonConvert.SerializeObject(JArray.Parse(tokens));
                        //send notification to VM made by subhaye
                        sendNotification(SP_NotiLog, VmServiceUrl, (string)param["NotiTitle"], (string)param["NotiBody"], tokenConvert, unreadCount, PgmSeq, PaperId, isEdit, senderName, CompanySeq, CreatedBy, receiverSeq, DsnBis);
                    }
                }
            }
            catch
            {
                //make errorDS
                //returnDS = createErrorDS(_SCAMobileNotiValidation + " : " + e.ToString());
            }

            return addXMLHeader(returnDS.GetXml());// + "request test";
        }
        public string replace(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Replace(@"'", @"''"); 
            }
            else
            {
                return value;
            }
        }
        public XElement createErrorDS(string ErrorMessage)
        {
            // Create DataTable instances.
            DataTable errTable = new DataTable("result");
            errTable.Columns.Add("ErrorState");
            errTable.Columns.Add("ErrorMessage");
            errTable.Rows.Add(0, ErrorMessage);

            // Create a DataSet and put table in it.
            DataSet errorDS = new DataSet("NewDataSet");
            errorDS.Tables.Add(errTable);
            return addXMLHeader(errorDS.GetXml());
        }
        public JObject callSP(string executeSP, string param, string serverName)
        {
            string requestURL = System.Web.HttpContext.Current.Request.Url.Scheme + "://" + System.Web.HttpContext.Current.Request.Url.Authority + "/MPExecuteSPSvc/api/sp";
            //string requestURL = "http://vxerp.kakaovx.com:8585/MPExecuteSPSvc/api/sp";
            //string requestURL = "http://wbs.ksystem.co.kr:8889/MPExecuteSPSvc/api/sp";
            //string requestURL = "http://ylwout.ksystem.co.kr:8585/MPExecuteSPSvc/api/sp";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(requestURL);

            httpWebRequest.ContentType = "application/json"; 
            httpWebRequest.Method = "POST";

            string response = "";

            JObject result = new JObject();
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                JObject jsonInfo = new JObject();
               
                try
                {
                    jsonInfo.Add("executesp", executeSP);
                    jsonInfo.Add("param", param);
                    jsonInfo.Add("servername", serverName);
                }
                catch (Exception e)
                {
                    result.Add("ErrorState", 0);
                    result.Add("ErrorMessage", e.ToString());
                    return result;
                }

                streamWriter.Write(jsonInfo);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    response = streamReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                result.Add("ErrorState", 0);
                result.Add("ErrorMessage", e.ToString());
                return result;
            }
            try
            {
                result = JObject.Parse(response);
                //Object jsonConvert = JsonConvert.DeserializeObject(response);

                //result = JObject.Parse((string)jsonConvert);
                //if (executeSP == _SCAMobileNotiValidation) {
                //    result = (string)json["Result"].First["Token"];
                //}
            }
            catch (Exception e)
            {
                result.Add("ErrorState", 0);
                result.Add("ErrorMessage", e.ToString());
                return result;
            }


            return result;
        }
        public XElement addXMLHeader(string value)
        {
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.CreateXmlDeclaration("1.0", "utf-16", null);
            try
            {
                //XDocument doc = XDocument.Parse(value);
                //doc.Declaration = new XDeclaration("1.0", "utf-16", "");
                //var element = XElement.Parse(value);
               
                return XElement.Parse(value);
            }
            catch
            {
                return createErrorDS("XML Parsing Error");
            }
        }
        //    public string GetData(int value)
        //    {
        //        return string.Format("You entered: {0}", value);
        //    }

        //    public CompositeType GetDataUsingDataContract(CompositeType composite)
        //    {
        //        if (composite == null)
        //        {
        //            throw new ArgumentNullException("composite");
        //        }
        //        if (composite.BoolValue)
        //        {
        //            composite.StringValue += "Suffix";
        //        }
        //        return composite;
        //    }
    }
}
